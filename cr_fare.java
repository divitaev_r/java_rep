class FareObject {
	public void init (int q, int p, int next) {
		_q = q;
		_p = p;
		_next = next;
	}
	
	public int q() {
		return _q;
	}
	
	public int p() {
		return _p;
	}
	
	public int next() {
		return _next;
	}
	
	public void setNext(int next) {
		_next = next;
	}
	
	private int _q, _p, _next;
};
/*
class FareList {
	public FareList (int number) {
		if(number <= 0) {
			return;
		}
		
		_fo = new FareObject [number];
		
		_point = 0;
		_number = number;
	}
	
	public boolean insert(FareObject fo) {
		if(_point + 1 >= _number) {
			return false;
		}
		
		_fo[_point] = fo;
		
		++_point;
		return true;
	}
	
	private FareObject [] _fo, _point, _number;
};
*/
static class Fare {
	public static void main (int argc, String [] argv) {
		int number = 10;
		
		FareObject fo = new FareObject [number*number];
		fo[0].init(0, 1, 1);
		fo[1].init(1, 1, 0);
		int val_number = 2;
		
		for(int i=1; i<number; ++i) {
			for(int j=0; j<val_number-1; ++j) {
				fo[val_number+j].init ( fo[j].q()+fo[fo[j].next()].q(),
										fo[j].p()+fo[fo[j].next()].p(),
										fo[fo[j].next()].next());
				fo[j].setNext(val_number+j);
			}
		}
	}
};