import java.util.ArrayList;

class FareObject {
	
	public FareObject (int q, int p, int next) {
		init (q, p, next);
	}
	
	public int q () {
		return _q;
	}
	
	public int p () {
		return _p;
	}
	
	public int next () {
		return _next;
	}
	
	public void setNext (int next) {
		_next = next;
	}
	
	private void init (int q, int p, int next) {
		_q = q;
		_p = p;
		_next = next;
	}
	
	private int _q, _p, _next;
};

public class hw_fare {
	public static void main (String [] args) {
		// лист для чисел Фарея
		ArrayList <FareObject> fare_list = new ArrayList <> ();
		
		// количество генерируемых чисел Фарея
		int fare_number = 7;
		
		// если количетсво генерируемых чисел меньше единицы, то
		// остальные преобразования не нужны
		if (fare_number > 0) {
			// создаем первое число Фарея (состоит из двух натуральных чисел)
			fare_list.add (new FareObject (0, 1, 1));
			fare_list.add (new FareObject (1, 1, 0));

			// цикл для создания остальных чисел Фарея (если требуется число 
			// больше единицы). первое число создано, поэтому цикл начинается
			// с i = 1
			for (int i=1; i<fare_number; ++i) {
				/* * * * * * * * * * * * * * * * * * * * * * * * * * * * *
				 * основные обозначения (в комментариях):
				 * Х - натуральное число, обрабатываемое на данный момент
				 * Y - натуральное число, на которое ссылается Х
				 * Z - натуральное число, получаемое при слиянии Х и Y
				 * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
				
				// количество натуральных чисел в числе Фарея на данный момент
				// для запуска цикла генерации других натуральных чисел,
				// входящих в число Фарея
				final int fare_list_cur_number = fare_list.size();
				for (int j=0; j<fare_list_cur_number; ++j) {
					// если Х ссылается на 0, то пропускаем обработку этого
					// натурального числа
					if (fare_list.get(j).next() != 0) {
						
						// новый числитель и знаменатель при слиянии X и Y
						
						int new_q = fare_list.get(j).q() + 
							fare_list.get(fare_list.get(j).next()).q();
						
						int new_p = fare_list.get(j).p() + 
							fare_list.get(fare_list.get(j).next()).p();
						
						// если новый числитель или новый знаменатель больше
						// основного количества чисел Фарея на данный момент
						// (i+1), то новое натуральное число не создается
						if (new_p <= i+1 && new_q <= i+1) {
							// иначе создается Z с ссылкой на Y, при этом X
							// меняет ссылку на Z
							fare_list.add (new FareObject (
								new_q,
								new_p,
								fare_list.get(j).next()));
							fare_list.get(j).setNext(fare_list.size()-1);
						}
					}
				}
			}
		}
		
		// вывод
		System.out.print("F (" + fare_number + ") = ");
		if (fare_number > 0) {
			System.out.print ((int)fare_list.get(0).q() + "/" 
				+ (int)fare_list.get(0).p());
			int link = fare_list.get(0).next();
			for (int i=1; i<fare_list.size(); ++i) {
				System.out.print (" + " + (int)fare_list.get(link).q() + "/" +
					(int)fare_list.get(link).p());
				link = fare_list.get(link).next();
			}
		} else {
			System.out.print ("non");
		}
		System.out.println ();
	}		
};